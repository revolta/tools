#! /bin/bash

#revolta_root_dir=$(dirname $(readlink -f "$0"))
cd ..
revolta_root_dir=${PWD}

default_binutils_version=2.40
default_gcc_version=13.1.0

default_source_dir="${HOME}/tmp/source"
default_build_dir="${HOME}/tmp/build"
default_log_dir="${HOME}/tmp/log"
default_install_dir="${revolta_root_dir}/toolchain"
default_sysroot_dir="${revolta_root_dir}/sysroot"

step_source_download=1

step_bootboot_source_download=1
step_bootboot_build=1
step_binutils_source_download=1
step_binutils_configuration=1
step_binutils_build=1
step_gcc_source_download=1
step_gcc_configuration=1
step_gcc_build=1
step_libgcc_build=1

for argument in "$@"
do
case $argument in
	--target=*)
IFS="-" read field1 field2 field3 field4 <<EOF
${argument#*=}
EOF
		case $1 in
			*-*)
				case $field1 in
					# x86 machine defined
					i[34567]86)	target_machine=$field1 ;;
					x86_64) target_machine=$field1 ;;
					*) echo "Unsupported  target machine ($field1) found"; exit ;;
				esac
						
				case $field2 in
					elf) target_os=$field2 ;;
					revolta) target_os=$field2 ;;
					*) echo "Unsupported  target os ($field2) found"; exit ;;
				esac
			;;
			
			*) echo "Unsupported --target definition ($1) found"; exit ;;
		esac
		shift
		;;
	--with-source-dir=*) source_dir="${argument#*=}" shift ;;
	--with-build-dir=*) build_dir="${argument#*=}" shift ;;
    --with-install-dir=*) install_dir="${argument#*=}" shift ;;
	--with-log-dir=*) log_dir="${argument#*=}" shift ;;
    --with-revolta-sysroot=*) sysroot_dir="${argument#*=}" shift ;;
    --with-binutils-version=*) binutils_version="${argument#*=}" shift ;;
    --with-gcc-version=*) gcc_version="${argument#*=}" shift ;;
	--skip-source-download) let step_source_download=0 shift ;;
	--skip-binutils-build) let step_binutils_build=0 shift ;;
	--skip-gcc-build) let step_gcc_build=0 shift ;;
	--skip-libgcc-build) let step_libgcc_build=0 shift ;;
    *) echo "Unknown option detected." exit ;;
esac
done

# Paralell build support - get the number of CPUs automatically
make_flags="-j $(nproc)"

#If binutils version is not specified, use the default one 
if [ ! ${binutils_version} ]; then binutils_version=${default_binutils_version}; fi

# If gcc_version is empty, set the default one
if [ ! ${gcc_version} ]; then gcc_version=${default_gcc_version}; fi

# If source_dir is empty, set the default one
if [ ! ${source_dir} ]; then source_dir=${default_source_dir}; fi
# If the build directory does not exist, make it 
if [ ! -d ${source_dir} ]; then mkdir -p ${source_dir} --verbose; fi

# If build_dir is empty, set the default one
if [ ! ${build_dir} ]; then build_dir=${default_build_dir}; fi
# If the build_dir does not exist, make it 
if [ ! -d ${build_dir} ]; then mkdir -p ${build_dir} --verbose; fi

# If install_dir is empty, set the default one
if [ ! ${install_dir} ]; then install_dir=${default_install_dir}; fi
# If the install_dir does not exist, make it 
if [ ! -d ${install_dir} ]; then mkdir -p ${install_dir} --verbose; fi

# If sysroot_dir is empty, set the default one
if [ ! ${sysroot_dir} ]; then sysroot_dir=${default_sysroot_dir}; fi
# If the sysroot_dir does not exist, make it
if [ ! -d ${sysroot_dir} ]; then mkdir -p ${sysroot_dir} --verbose; fi

# If log_dir is empty, set the default one
if [ ! ${log_dir} ]; then log_dir=${default_log_dir}; fi
# If the log_dir does not exist, make it
if [ ! -d ${log_dir} ]; then mkdir -p ${log_dir} --verbose; fi

# Create sysroot internal directory structure
mkdir -p "${sysroot_dir}/usr/include"
mkdir -p "${sysroot_dir}/usr/lib"

bootboot_source_dir=${source_dir}/bootboot
binutils_source_dir=${source_dir}/binutils_${binutils_version}
gcc_source_dir=${source_dir}/gcc_${gcc_version}

binutils_build_dir=${build_dir}/binutils_${binutils_version}_$target_machine-$target_os
gcc_build_dir=${build_dir}/gcc_${gcc_version}_$target_machine-$target_os

# Clean up environment
# TODO

# BOOTBOOT source download
if [ ${step_bootboot_source_download} == 1 ]; then
	mkdir -p ${bootboot_source_dir}

	# Clone the sources
	git clone https://gitlab.com/bztsrc/bootboot.git ${bootboot_source_dir}
fi

# BOOTBOOT mkbootimg tool build and install
if [ ${step_bootboot_build} == 1 ]; then
	# Build
	echo -n "Building BOOTBOOT mkbootimg..."
	cd ${bootboot_source_dir}/mkbootimg
	make >> ${log_dir}/bootboot_mkbootimg_build.log 2>&1
	if [ $? -ne 0 ]; then echo "[ FAILED ]" ; exit 1; else echo "[ OK ]"; fi
	mkdir -p ${install_dir}/bootboot/ && mv ./mkbootimg ${install_dir}/bootboot/
fi

# Binutils source download and patching
if [ ${step_binutils_source_download} == 1 ]; then
	# Create source dir
	mkdir -p ${binutils_source_dir}
	# Prepare requested version in correct string form
	binutils_version_str=${binutils_version//./_}

	# Clone the sources
	git clone --branch binutils-${binutils_version_str}-branch --single-branch --depth 1 git://sourceware.org/git/binutils-gdb.git ${binutils_source_dir}

	# Patch binutils by ReVolta specific patch
	if [ ${target_os} = "revolta" ]; then
		echo -n "Patching Binutils ${binutils_version} by ReVolta specifics: binutils_${binutils_version}.patch ...";
		cd ${binutils_source_dir} && git apply ${revolta_root_dir}/tools/patch/binutils_${binutils_version}.patch
		if [ $? -ne 0 ]; then echo "[ FAILED ]" ; exit 1; else echo "[ OK ]"; fi
		
		# TODO: Find a way how to determine required automake version automatically
		echo -n "Running Automake in ${binutils_source_dir}/ld directory ..."
		cd ${binutils_source_dir}/ld && automake-1.15
		if [ $? -ne 0 ]; then echo "[ FAILED ]" ; exit 1; else echo "[ OK ]"; fi
	fi
fi

# Configure Binutils
if [ ${step_binutils_configuration} == 1 ]; then
	# TODO: Check the presence of binutils source
	echo -n "Creating Binutils ${binutils_version} build directory: ${binutils_build_dir} ...";
	# Create build dir
	mkdir -p ${binutils_build_dir}
	if [ $? -ne 0 ]; then echo "[ FAILED ]" ; exit 1; else echo "[ OK ]"; fi

	echo -n "Configuring Binutils ${binutils_version} ..."
	cd ${binutils_build_dir}
	# Configure
	${binutils_source_dir}/configure \
		--target=${target_machine}-${target_os} \
		--prefix=${install_dir}/${target_machine}-${target_os} \
		--with-sysroot \
		--disable-nls \
		--disable-werror \
		--enable-64-bit-bfd \
		--with-python \
		>> ${log_dir}/binutils${binutils_version}_configure.log 2>&1
	# Check whether the configuration ended successfully. If not, exit
	if [ $? -ne 0 ]; then echo "[ FAILED ]" ; exit 1; else echo "[ OK ]"; fi
fi

# Build binutils
if [ ${step_binutils_build} == 1 ]; then
	# Build
	echo -n "Building Binutils ${binutils_version} ..."
	make ${make_flags} >> ${log_dir}/binutils${binutils_version}_build.log 2>&1
	# Check whether the build ended successfully. If not, exit
	if [ $? -ne 0 ]; then echo "[ FAILED ]" ; exit 1; else echo "[ OK ]"; fi

	# Install
	echo -n "Installing Binutils ${binutils_version} in ${install_dir} ..."
	make install >> ${log_dir}/binutils${binutils_version}_install.log 2>&1
	# Check whether the installation ended successfully. If not, exit
	if [ $? -ne 0 ]; then echo "[ FAILED ]" ; exit 1; else echo "[ OK ]"; fi
fi

# GCC source download
if [ ${step_gcc_source_download} == 1 ]; then
	# Create source dir
	mkdir -p ${gcc_source_dir}
	# Clone the sources
	git clone --branch releases/gcc-${gcc_version} --single-branch --depth 1 git://gcc.gnu.org/git/gcc.git ${gcc_source_dir}
	# Download GCC prerequisities
	cd ${gcc_source_dir} && ./contrib/download_prerequisites

	if [ ${target_os} = "revolta" ]; then
		echo -n "Patching GCC ${gcc_version} by ReVolta specifics: gcc_${gcc_version}.patch ...";
		cd ${gcc_source_dir} && git apply ${revolta_root_dir}/tools/patch/gcc_${gcc_version}.patch
		if [ $? -ne 0 ]; then echo "[ FAILED ]" ; exit 1; else echo "[ OK ]"; fi
		
		echo -n "Running autoconf in 'libstdc++-v3' directory ...";
		cd ${gcc_source_dir}/libstdc++-v3 && autoconf
		if [ $? -ne 0 ]; then echo "[ FAILED ]" ; exit 1; else echo "[ OK ]"; fi
		
		case ${target_machine} in
			i[3-6]86)
				target_processor="x86"
				toolchain_triplet=${target_machine}-elf
				;;
			x86_64)
				target_processor="x86_64"
				toolchain_triplet=${target_machine}-elf
				;;
			*) echo "Not supported machine defined."; exit ;;
		esac

		# Define the directory where to build libc
		toolchain_libc_build_dir=${revolta_root_dir}/build/toolchain-${toolchain_triplet}
		echo "Using '${toolchain_triplet}' toolchain to build ReVolta libc in ${toolchain_libc_build_dir}"
		# Create the directory
		mkdir -p ${toolchain_libc_build_dir}
		# Configure ReVolta for libc build and headers installations
		echo -n "Configuring ReVolta ...";
		cmake -E chdir ${toolchain_libc_build_dir} \
		cmake ${revolta_root_dir} -G"Unix Makefiles" \
			-DCMAKE_TARGET_PROCESSOR=${target_processor} \
			-DREVOLTA_TOOLCHAIN_TRIPLET=${toolchain_triplet} \
			-DCMAKE_BUILD_TYPE=Debug \
			>> ${log_dir}/revolta_libc_configure.log 2>&1
		# Check whether the configuration ended successfully. If not, exit
		if [ $? -ne 0 ]; then echo "[ FAILED ]" ; exit 1; else echo "[ OK ]"; fi
		
		echo -n "Installing libc and headers to sysroot_dir (${sysroot_dir}) ..."
		cd ${toolchain_libc_build_dir} && make install_libc_headers install_pthread_headers \
		>> ${log_dir}/revolta_libc_build.log 2>&1
		# Check whether the headers installations ended successfully. If not, exit
		if [ $? -ne 0 ]; then echo "[ FAILED ]" ; exit 1; else echo "[ OK ]"; fi
	fi
fi

# Configure GCC
if [ ${step_gcc_configuration} == 1 ]; then
	# TODO: Check the presence of binutils source
	# Create build dir
	echo -n "Creating GCC ${gcc_version} build directory: ${gcc_build_dir} ...";
	mkdir -p ${gcc_build_dir}
	if [ $? -ne 0 ]; then echo "[ FAILED ]" ; exit 1; else echo "[ OK ]"; fi

	case ${target_os} in
		elf)
			echo -n "Configuring GCC ${gcc_version} ..."
			# Configure *-elf GCC build
			cd ${gcc_build_dir}
			${gcc_source_dir}/configure \
				--target=${target_machine}-${target_os} \
				--prefix=${install_dir}/${target_machine}-${target_os} \
				--disable-nls \
				--enable-languages=c,c++ \
				--without-headers \
				>> ${log_dir}/gcc_${gcc_version}_${target_machine}-${target_os}_configure.log 2>&1
			if [ $? -ne 0 ]; then echo "[ FAILED ]" ; exit 1; else echo "[ OK ]"; fi
		;;
			
		revolta)
			# Configure *-revolta GCC build
			echo -n "Configuring GCC ${gcc_version} ..."
			cd ${gcc_build_dir}
			${gcc_source_dir}/configure \
				--target=${target_machine}-${target_os} \
				--prefix=${install_dir}/${target_machine}-${target_os} \
				--disable-nls \
				--enable-languages=c,c++ \
				--with-sysroot=${sysroot_dir} \
				>> ${log_dir}/gcc_${gcc_version}_${target_machine}-${target_os}_configure.log 2>&1
			# Check whether the configuration ended successfully. If not, exit 
			if [ $? -ne 0 ]; then echo "[ FAILED ]" ; exit 1; else echo "[ OK ]"; fi
		;;
	esac
fi

# Build GCC
if [ ${step_gcc_build} == 1 ]; then
	# Build gcc
	echo -n "Building GCC ${gcc_version} ...";
	make ${make_flags} all-gcc >> ${log_dir}/gcc_${gcc_version}_${target_machine}-${target_os}_build.log 2>&1
	# Check whether the build ended successfully. If not, exit
	if [ $? -ne 0 ]; then echo "[ FAILED ]" ; exit 1; else echo "[ OK ]"; fi

	# Install gcc
	echo -n "Installing GCC ${gcc_version} ...";
	make ${make_flags} install-gcc >> ${log_dir}/gcc_${gcc_version}_${target_machine}-${target_os}_install.log 2>&1
	# Check whether the build ended successfully. If not, exit
	if [ $? -ne 0 ]; then echo "[ FAILED ]" ; exit 1; else echo "[ OK ]"; fi
fi

# FIXME: GCC configure must be run prior to building libgcc...
# Build libgcc
if [ ${step_libgcc_build} == 1 ]; then
	# TODO: Check the presence of binutils source
	# Build libgcc
	echo -n "Building libgcc ...";
	make ${make_flags} all-target-libgcc >> ${log_dir}/libgcc_${gcc_version}_${target_machine}-${target_os}_build.log 2>&1
	# Check whether the build ended successfully. If not, exit
	if [ $? -ne 0 ]; then echo "[ FAILED ]" ; exit 1; else echo "[ OK ]"; fi

	# Install libgcc
	echo -n "Installing libgcc ...";
	make ${make_flags} install-target-libgcc >> ${log_dir}/libgcc_${gcc_version}_${target_machine}-${target_os}_install.log 2>&1
	# Check whether the build ended successfully. If not, exit
	if [ $? -ne 0 ]; then echo "[ FAILED ]" ; exit 1; else echo "[ OK ]"; fi
fi

echo "Toolchain built and installed successfully :)"
