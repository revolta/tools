# Tools

## Install build dependencies
In order to build ReVolta toolchain, the following dependencies must be installed in your system:

    sudo apt install build-essential bison flex libgmp3-dev libmpc-dev libmpfr-dev texinfo libisl-dev libexpat1-dev python-dev-is-python3

### CMake
`CMake` is used as build system for ReVolta. The minimum version required is defined in ReVolta's root directory `CMakeLists.txt`. It can be either installed from distribution repositories. The preferred way is to install `CMake` using `snap`:

    sudo snap install cmake --classic

### ReVolta ISO support

    sudo apt install xorriso mtools grub-common

### QEMU

    sudo apt install qemu-system-x86

### GoogleTest
GoogleTest is testing framework used to unit test ReVolta. First of all the GoogleTest package shall be installed:

    sudo apt install libgtest-dev

The package `libgtest-dev` installs just the source files which must be build prior usage:

    cd /usr/src/gtest
    sudo cmake CMakeLists.txt
    sudo make

There are two libraries build using the commands above. They can be both found within the `lib` directory. To make them usable, copy or symlink then to your `/usr/lib` directory

    cd ./lib
    sudo cp *.a /usr/lib

### Python support
    sudo apt install python3-pip

### Automake 1.15.1
`automake 1.15.1` is required in order to build ReVolta OS specific toolchain. If not found in distro depositories, you can build and install it from source:

    wget https://ftp.gnu.org/gnu/automake/automake-1.15.1.tar.xz
    tar -xf ./automake-1.15.1.tar.xz
    cd automake-1.15.1/
    ./configure
    make
    sudo make install

### Autoconf 2.69
`autoconf 2.69` is required to apply ReVolta specific patch to GCC

    wget https://ftp.gnu.org/gnu/autoconf/autoconf-2.69.tar.xz
    tar -xf ./autoconf-2.69.tar.xz
    cd autoconf-2.69/
    ./configure
    make
    sudo make install

Alternatively, you can install the aforementioned `automake` and `autoconf` versions in paralell to distro provided versions and setup the `PATH` variable the way that the explicit versions as mentioned above are prepended to the content of `PATH` environment variable:

    export PATH=/usr/local/bin/:$PATH

## Building cross compiler
Cross compiler is mainly used to build ReVolta OS specific compiler.

    ./toolchain.sh --target=i686-elf

## Building ReVolta OS specific compiler
Cross compiler is mainly used to build ReVolta OS specific compiler.

    ./toolchain.sh --target=i686-revolta
