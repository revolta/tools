#!/bin/sh

start_debug_session=false

for argument in "$@"
do
case $argument in
    --with-project-root-dir=*) project_root_dir="${argument#*=}" shift ;;
    --debug*) start_debug_session=true shift ;;
esac
done

if [ ! ${project_root_dir} ]; then echo "Project root directory not specified. Set '--with-project-root-dir'"; exit; fi

# Qemu
# Required installation: '$ sudo apt install qemu-system-x86 qemu-system-arm' 
if [ ${start_debug_session} = true ]; then
	qemu-system-x86_64 -bios /usr/share/ovmf/OVMF.fd -smp 4 -m 128M -drive file=revolta-x86_64.img,format=raw -no-reboot -no-shutdown -gdb tcp::1234 -S
else
	# See: https://fadeevab.com/how-to-setup-qemu-output-to-console-and-automate-using-shell-script/
	qemu-system-x86_64 -bios /usr/share/ovmf/OVMF.fd -smp 4 -m 128M -drive file=revolta-x86_64.img,format=raw -no-reboot -no-shutdown
fi

# LIST OF QEMU MONITOR COMMANDS 
# https://doc.opensuse.org/documentation/leap/archive/42.3/virtualization/html/book.virt/cha.qemu.monitor.html

# To start debugging, run the following command in the terminal window:
#gdb ./iso/boot/kernel.elf -ex 'target remote localhost:1234' -ex 'b kernel_main'

